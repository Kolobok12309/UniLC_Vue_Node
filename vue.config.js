module.exports = {
    baseUrl: undefined,
    outputDir: undefined,
    assetsDir: 'sys',
    runtimeCompiler: undefined,
    productionSourceMap: undefined,
    parallel: undefined,
    css: undefined,
    configureWebpack: {},
    transpileDependencies: [],
};
