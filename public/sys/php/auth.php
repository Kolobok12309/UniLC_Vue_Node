<?php
	require('lib/lib.php');
	require('lib/admin.lib.php');

	$reqObj = json_decode(file_get_contents('php://input'));
	/**
	 * Проверка полей
	 * @param string $name название проверка
	 * @param string $text текст для проверки
	 * @return boolean true - соответствует, false - не соответствует
	 */
	function check_pm($name,$text) {
		switch ($name) {
			case 'fullname':
				$check = preg_match('/^[А-Я]{1}[а-я]{1,}\s[А-Я]{1}[а-я]{1,}\s[А-Я]{1}[а-я]{1,}$/u', $text);
				break;
			case 'login':
				$check = preg_match('/^([A-Za-z\d]|_(?!_)|\-(?!\-)){5,12}$/', $text);
				break;
			case 'group':
				$check = preg_match('/^[А-Я]{2}[С,Б]{1}[О,З]{1}\-[0-9]{2}\-[0-9]{2}$/u', $text);
				break;
			case 'cipher':
				$check = preg_match('/^[0-9]{2}[А-Я]{1}[0-9]{4}$/u', $text);
				break;
		}
		return $check;
	}
	// login пользователя
	function login() {
		global $reqObj;
		$login = antihack($reqObj->login);
		if ((check_pm('login',$login)) || ($login=='user')) {
			$db = open_db();
			$row = $db->querySingle("SELECT id,hash,rights,banned FROM users WHERE login='$login'",true);
			$db->close();
			if (!$row['banned']) {
				if (password_verify($reqObj->password, $row['hash'])) {
					$_SESSION = ["userid"=>$row['id'],"rights"=>$row['rights']];
					echo json_encode(["status" => 'ok',"user"=>["login"=>$login,"id"=>$row['id'],"rights"=>$row['rights']]]);
					write_log('info','loggined');
				} else echo json_encode(["status"=>"err","value"=>"passwd","msg"=>"Не правильный логин или пароль!"]);
			} else echo json_encode(["status"=>"err","value"=>"banned","msg"=>"Пользователь заблокирован"]);
		} else echo json_encode(["status"=>"err","value"=>"pregmatch","msg"=>"Недопустимый логин!"]);
	}
	// 
	/**
	 * регистрация пользователя
	 * @return [type] [description]
	 */
	function registration() {
		$db = open_db();
		$request = antihack(["login" => $_POST['login'],"chipher" => $_POST['cipher'],"fullname" => $_POST['fullname'],"group" => $_POST['group']]);
		$group_small = mb_strimwidth($request['group'], 0, 4);
		if ($settings['menu']['register']){
			if (check_pm('login',$login) && check_pm('fullname',$fullname) && check_pm('group',$group) && check_pm('cipher',$cipher)) {
				$db_check_login = $db->querySingle("SELECT login FROM users WHERE login='$login'");
				$db_check_cipher = $db->querySingle('SELECT `cipher` FROM `users_info` WHERE cipher="'.$cipher.'"');
				if ($db_check_login != $login) {
					if ($db_check_cipher != $cipher) {
						$db->exec('INSERT INTO `users` VALUES (NULL,"'.$login.'","'.password_hash($_POST['pass'], PASSWORD_DEFAULT).'","usr",NULL)');
						$id = $db->querySingle('SELECT id FROM users WHERE user="'.$login.'"');
						$db->exec('INSERT INTO `users_info` VALUES ('.$id.',"'.$fullname.'","'.$group.'","'.$cipher.'",NULL,'.mb_strimwidth($group, -2, 2).',0)');
						$check_new_group = $db->querySingle('SELECT "group" FROM groups WHERE "group"="'.mb_strimwidth($group, 0, 4).'"');
						if ($check_new_group != mb_strimwidth($group, 0, 4)) {
							$db->exec('INSERT INTO `groups` VALUES (NULL,"'.mb_strimwidth($group, 0, 4).'",NULL)');
						}
						echo "ok";
						write_log('info','new user: '.$login.'['.$id.']');
					} else "Пользователь с таким шифром уже существует!";
				} else echo "Пользователь с таким именем уже существует!";
				$db->close();
			} else echo "Проверьте правильность ввода данных!";
		} else echo $settings['menu']['r_msg'];
	}
	/**
	 * разрушает сессию пользователя
	 */
	function logout() {
		if (session_destroy()) {
			echo json_encode(["status"=>"ok"]);
			write_log('info','logged out');
		} else {
			echo json_encode(["status"=>"err","value"=>"session","msg"=>"Не удалось выйти :с"]);
			write_log('error','not logged out');
		}
	}

	function getSelfUser() {
		if(!empty($_SESSION['userid'])) {
			$id = $_SESSION['userid'];
			$db = open_db();
			$row = $db->querySingle("SELECT rights,login FROM users WHERE id='$id'",true);
			$db->close();
			echo json_encode(["status"=>"ok","user"=>["login"=>$row["login"],"rights"=>$row["rights"],"id"=>$id]]);
		} else {
			echo json_encode(["status"=>"error"]);
		}
	}

	function getUserTable() {
		$db = open_db();
		$tableUsers = $db->query("SELECT id,login,rights,fullname FROM users");
		$users = array();
		while ($row = $tableUsers->fetchArray(SQLITE3_ASSOC)) {
			$users[] = $row;
		}
		$db->close();
		echo json_encode(["status"=>"ok","users" => $users]);
	}

	function getFullUser() {
		global $reqObj;
		$info = get_accout_info($reqObj -> id);
		echo json_encode(["status" => "ok", "user" => $info]);
	}

	if($reqObj) {
		switch ($reqObj->action) {
			case 'login':
				login();
				break;
			case 'registration':
				register();
				break;
			case 'logout':
				logout();
				break;
			case 'getSelfUser':
				getSelfUser();
				break;
			case 'getUserTable':
				getUserTable();
				break;
			case 'getFullUser':
				getFullUser();
				break;
			default:
				// go_out();
				break;
		}
	}
?>