<?php
	require 'Parsedown.php';
	session_start();
	set_time_limit(120);
	header("Access-Control-Allow-Origin: *",false);
	header("Access-Control-Allow-Headers: *",false);
	header("Content-Type: application/json",false);

	// Глобальная переменная настроек
	$settings = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/sys/settings.ini");
	$sys = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/sys/app.ini");
	/**
	 * Создает экземпляр базыданных с оптимизацией и прописанным путем
	 * @return object возвращает экземпляр открытой базы данных
	 */
	function open_db() {
		$db = new SQLite3("{$_SERVER['DOCUMENT_ROOT']}/sys/storage.db");
		$db->busyTimeout(5000); // Оптимизация БД
		$db->exec('PRAGMA journal_mode=WAL;'); // Оптимизация БД
		return $db;
	}
	/**
	 * Функция защиты от частых атак
	 * @param mixed $text обрабатываемый текст
	 * @return string экранированный текст
	 */
	function antihack($text) {
		if (is_array($text)) {
			foreach (func_get_args() as $value) {
				$value = strip_tags(htmlspecialchars(SQLite3::escapeString($value)));
			}
		} else $text = strip_tags(htmlspecialchars(SQLite3::escapeString($text)));
		return $text;
	}
	function mdbody($text) {
		$Parsedown = new Parsedown();
		return $Parsedown->text($text);
	}
	/**
	 * Записывает ошибку в sys.log
	 * @param string $level Уровень ошибки
	 * @param string $msg Сообщение ошибки
	 */
	function write_log($level, $msg) {
		$fp = fopen('../sys.log', 'a');
		$date = '['.date('d-m-Y H:i:s').']';
		$level = '['.$level.']';
		if (!empty($_SESSION['userid'])) {
			$who = '['.$_SESSION['userid'].']';
		} else $who = '[NaN]';
		if (empty($msg)) $msg = "Unknown err...";
		$check = fwrite($fp, $date.$level.$who.' - '.$msg."\n");
		if (!$check) echo "Не удалось записать лог!";
		fclose($fp);
	}
	/**
	 * автовыход из системыных файлов
	 * @param string $msg сообщение возврата
	 */
	function go_out($msg = NULL) {
		$protocol = (!empty($_SERVER['HTTPS']) && 'off' !== strtolower($_SERVER['HTTPS']))? "https":"http";
		if (empty($msg)) {
			header("Location: ".$protocol."://".$_SERVER['HTTP_HOST'],false);
			write_log("warning","Попытка входа в системные файлы!");
		} else header("Location: ".$protocol."://".$_SERVER['HTTP_HOST'].$msg);
		exit;
	}
	/**
	 * возвращает соответствие текущим правам доступа
	 * @param string $right три типа прав: admin, teacher, editor
	 * @return boolean true - соответствует, false не соответствует
	 */
	function check_rights($right) {
		switch ($right) {
			case 'admin':
				$check = ($_SESSION['role'] === "admin")? true:false;
				break;
			case 'teacher':
				$check = (($_SESSION['role'] === "admin") || ($_SESSION['role'] === "teacher"))? true:false;
				break;
			case 'editor':
				$check = (($_SESSION['role'] === "admin") || ($_SESSION['role'] === "editor") || ($_SESSION['role'] === "teacher"))? true:false;
				break;
			default:
				$check = false;
				break;
		}
		return $check;
	}
	/**
	 * функция проверки прав пользователя
	 * @param string $... три типа прав: admin, teacher, editor
	 * @return mixed true - соответствует, false - не соответствует, null - такого типа прав нету
	 */
	// function check_rights() {
	// 	$check = null;
	// 	foreach (func_get_args() as $value) {
	// 		switch ($value) {
	// 			case 'admin':
	// 				$check = (($_SESSION['role'] === "admin") && ($check != false)) ? true : false;
	// 				break;
	// 			case 'teacher':
	// 				$check = (($_SESSION['role'] === "teacher") && ($check != false)) ? true : false;
	// 				break;
	// 			case 'editor':
	// 				$check = (($_SESSION['role'] === "editor") && ($check != false)) ? true : false;
	// 				break;
	// 			default:
	// 				return null;
	// 		}
	// 	}
	// 	return $check;
	// }
	function get_accout_info($id) {
		$db = open_db();
		$info = $db->querySingle("SELECT id,login,fullname,cipher,verified,subgroup,vkid,email,rights FROM users WHERE id=$id",true);
		$db->close();
		return $info;
	}
?>