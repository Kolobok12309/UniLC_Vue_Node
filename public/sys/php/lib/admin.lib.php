<?php
	require_once 'lib.php';
	function get_user_table() {
		$db = open_db();
		$table = array();
		$users = $db->query("SELECT id,login,rights FROM users");
		while ($row = $users->fetchArray(SQLITE3_ASSOC)) {
			$table[] = "
				<tr>
					<td>{$row['id']}</td>
					<td>
						<form action='/settings/usertable/account' method='POST' style='margin: 0;'>
							<input name='id' value='{$row['id']}' type='hidden'/>
							<button type='submit' value='getuserprofile' style='margin: 0; border: 0; color: #2196f3;'>{$row['login']}</button>
						</form>
					</td>
					<td>{$row['rights']}</td>
				</tr>
			";
		}
		$db->close();
		return implode($table);
	}
	function create_debug_user($user) {
		function create_user($user,$fio) {
			$pass = password_hash("123", PASSWORD_DEFAULT);
			$cipher = rand(0,999);
			$db = open_db();
			$status = $db->exec("INSERT INTO users VALUES (NULL,'$user','$pass','$user','$fio',$cipher,0,1,'XXXX-00-00',NULL,NULL)");
			$db->close();
			echo ($status)?json_encode(["status" => "ok"]):json_encode(["status"=>"err","value"=>"errcreate","msg"=>"Не возможно создать $user!"]);
		}
		switch ($user) {
			case 'user':
				create_user($user,"Юзер Всея Юзерович");
				break;
			case 'editor':
				create_user($user,"Эдитор Всея Эдиторович");
				break;
			case 'teacher':
				create_user($user,"Тичер Всея Тичерович");
				break;
			default:
				echo json_encode(["status"=>"err","value"=>"username","msg"=>"Недопустимое имя $user!"]);
				break;
		}
	}
	function remove_debug_user($user) {
		function remove_user($user) {
			$db = open_db();
			$status = $db->exec("DELETE FROM users WHERE login='$user'");
			$db->close();
			if ($status) {
				echo json_encode(["status" => "ok"]);
			} else echo json_encode(["status"=>"err","value"=>"usernf","msg"=>"Не возможно удалить $user, скорее всего он был уже удален!"]);
		}
		switch ($user) {
			case 'user':
				remove_user($user);
				break;
			case 'editor':
				remove_user($user);
				break;
			case 'teacher':
				remove_user($user);
				break;
			default:
				echo json_encode(["status"=>"err","value"=>"username","msg"=>"Недопустимое имя $user!"]);
				break;
		}
	}
	function check_debug_users() {
		$db = open_db();
		$check['user'] = $db->querySingle("SELECT login FROM users WHERE login='user'");
		$check['editor'] = $db->querySingle("SELECT login FROM users WHERE login='editor'");
		$check['teacher'] = $db->querySingle("SELECT login FROM users WHERE login='teacher'");
		$db->close();
		foreach ($check as &$value) $value = (empty($value))?false:true;
		echo json_encode(["status"=>"ok","check" => $check]);
	}
	function rebase() {}
	function clear_logs() {
		if (file_exists("../sys.log")) {
			if (unlink("../sys.log")) {
				write_log('info','Логи очищены!');
				echo json_encode(["status"=>"ok","msg"=>"Логи очищены!"]);
			} else echo json_encode(["status"=>"err","value"=>"clearlog","msg"=>"Не удалось очистить логи!"]);
		} else echo json_encode(["status"=>"err","value"=>"fileexists","msg"=>"Логи уже отчищены! :)"]);
	}
	function get_logs() {
		if (file_exists("../sys.log")) {
			echo json_encode(["status"=>"ok","values"=>file_get_contents("../sys.log")]);
		} else {
			echo json_encode(["status"=>"err","value"=>"getlogs","msg"=>"Не удалось прочитать логи!"]);
		}
	}
?>