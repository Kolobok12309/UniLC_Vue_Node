<?php
	include 'lib/lib.php';
	include 'lib/admin.lib.php';
	switch ($_POST['action']) {
		case 'createuser':
			create_debug_user($_POST['user_type']);
			break;
		case 'removeuser':
			remove_debug_user($_POST['user_type']);
			break;
		case 'checkuser':
			check_debug_users();
			break;
		case 'rebase':
			rebase();
			break;
		case 'clearlogs':
			clear_logs();
			break;
		case 'getlogs':
			get_logs();
			break;
		default:
			go_out();
			break;
	}
?>