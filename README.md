# UniLC_Vue_Node

В этом репо лежат файлы для уже готового компилирования и размещения на сервер

## Установка
Открыть консоль в главной папке и написать
```
npm install
```
После этого поставятся все необходимые пакеты

## Сборка
Для сборки и полноценной проверки на production, файлы будут лежать в папке dist
```
npm run build
```
## Режим разработки
1. npm install
2. В файле src/network.js поменять адрес локалхоста
3. npm run build
4. Из папки dist достать файлы и перебросить на свой веб-сервер
5. npm run serve
6. Адрес localhost:8080
```
npm run serve
```

Для нормальной работы History API нужно настроить веб-сервер на котором будете хостить [click](https://router.vuejs.org/ru/guide/essentials/history-mode.html#%D0%BF%D1%80%D0%B8%D0%BC%D0%B5%D1%80%D1%8B-%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B3%D1%83%D1%80%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-%D1%81%D0%B5%D1%80%D0%B2%D0%B5%D1%80%D0%BE%D0%B2)
Альтернативой можно использовать #, для этого в src/router.js в строке заменить history на hash
```
    mode: 'history',
```