import Vue from 'vue';
import Router from 'vue-router';
import store from './store';
import routes from './routes';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes,
});

router.beforeEach((to, from, next) => {
    const myRights = store.state.Auth.User.rights;
    if (!to.matched.some(route => route.meta.access && route.meta.access.every(role => !(role === 'all' || role === myRights || (role === 'auth' && myRights !== null))))) {
        next();
    } else {
        next('/');
    }
});

export default router;
