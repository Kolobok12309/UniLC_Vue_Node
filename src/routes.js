import Tests from './views/Tests.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';
import Main from './views/Main.vue';
import Settings from './views/settings';
import Example from './views/Example.vue';

export default [
    {
        path: '/',
        name: 'Main',
        component: Main,
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            access: [null],
        },
    },
    {
        path: '/example',
        name: 'Example',
        component: Example,
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
            access: [null],
        },
    },
    {
        path: '/tests',
        name: 'tests',
        component: Tests,
        meta: {
            access: ['auth'],
        },
    },
    {
        path: '/settings',
        component: Settings.frameSettings,
        meta: {
            access: ['auth'],
        },
        children: [
            {
                path: '',
                name: 'Settings',
                component: Settings.mainSettings,
                meta: {
                    title: 'Настройки',
                },
            },
            {
                path: 'account',
                name: 'Account',
                component: Tests,
                meta: {
                    title: 'Аккаунт',
                },
            },
            {
                path: 'about',
                name: 'About',
                component: Settings.about,
                meta: {
                    title: 'О портале',
                },
            },
            {
                path: 'usertable',
                component: Settings.simpleFrame,
                meta: {
                    access: ['admin'],
                },
                children: [
                    {
                        path: '',
                        name: 'UserTable',
                        component: Settings.userTable,
                        meta: {
                            title: 'Пользователи',
                            access: ['admin'],
                        },
                    },
                    {
                        path: 'account/id:id',
                        name: 'userProfile',
                        component: Settings.userProfile,
                        meta: {
                            title: 'Аккаунт',
                            access: ['admin'],
                        },
                    },
                ],
            },
        ],
    },
    {
        path: '*',
        name: 'default',
        redirect: '/',
    },
];
