import simpleFrame from '@/components/frame.vue';
import mainSettings from './settings.vue';
import frameSettings from './frame.vue';
import about from './about.vue';
import userTable from './admin/userTable.vue';
import userProfile from './admin/userProfile.vue';

export default {
    mainSettings,
    frameSettings,
    about,
    userTable,
    simpleFrame,
    userProfile,
};
