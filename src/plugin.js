import myFooter from '@/components/footer.vue';
import backMenu from '@/components/menus/backmenu.vue';
import routerBack from '@/components/routerBack.vue';
import frame from '@/components/frame.vue';

export default {
    install(Vue) {
        Vue.component('myFooter', myFooter);
        Vue.component('backMenu', backMenu);
        Vue.component('router-back', routerBack);
        Vue.component('myFrame', frame);
        Vue.mixin({
            methods: {
                checkRights(rightsArg) {
                    const a = this.$store.state.rights.indexOf(rightsArg);
                    const b = this.$store.state.rights.indexOf(this.$store.state.Auth.User.rights);
                    return a <= b;
                },
                getRights() {
                    return this.$store.state.Auth.User.rights;
                },
                logout() {
                    this.$store.dispatch('AUTH_LOGOUT').then(() => {
                        this.$router.push('/login');
                    }).catch((err) => {
                        console.log(err);
                    });
                },
            },
        });
    },
};
