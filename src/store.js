import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import myLib from '@/myLib';

Vue.use(Vuex);

const User = myLib.getInf();

const url = '';

export default new Vuex.Store({
    state: {
        Auth: {
            User: {
                id: User.id,
                login: User.login,
                rights: User.rights,
            },
            timer: null,
            Status: null,
        },
        time: {
            tableUsersTimer: null,
            tableUsersUpdate: 0,
        },
        rights: [null, 'user', 'editor', 'teacher', 'admin'],
        users: [User],
        errors: [],
        lectures: {},
    },
    mutations: {
        AUTH_TIMER: (state, timer) => {
            state.Auth.timer = timer;
        },
        AUTH_REQ: (state) => {
            state.Auth.Status = 'loading';
            myLib.delInf();
            state.Auth.User.id = null;
            state.Auth.User.login = null;
            state.Auth.User.rights = null;
        },
        AUTH_FAIL: (state) => {
            state.Auth.Status = 'error';
            myLib.delInf();
            state.Auth.User.id = null;
            state.Auth.User.login = null;
            state.Auth.User.rights = null;
        },
        AUTH_SUCCESS: (state, user) => {
            state.Auth.Status = true;
            myLib.setInf(user);
            state.Auth.User.id = user.id;
            state.Auth.User.login = user.login;
            state.Auth.User.rights = user.rights;
        },
        AUTH_LOGOUT: (state) => {
            myLib.delInf();
            state.Auth.User.id = null;
            state.Auth.User.login = null;
            state.Auth.User.rights = null;
            state.Auth.status = 'exited';
        },
        PERMISSION_DENIED: (state, path) => {
            state.errors.push({
                type: 'Perm_denied',
                path,
            });
        },
        GET_USERS: (state, users) => {
            Vue.set(state, 'users', myLib.myConcat(state.users, users));
            // state.users = users.concat();
            state.time.tableUsersUpdate = new Date() - 0;
        },
        GET_FULL_USER: (state, newUser) => {
            const olduser = state.users.findIndex(user => user.id === newUser.id);
            if (olduser === -1) state.users.push(newUser);
            else if (!state.users[olduser].cipher) Vue.set(state.users, olduser, newUser);
            else myLib.concatObj(state.users[olduser], newUser);
        },
        PLACE_USER: (state, user) => {
            state.users.push(user);
        },
    },
    actions: {
        AUTH_REQ: ({ commit }, user) => new Promise((res, rej) => {
            commit('AUTH_REQ');
            axios.post(`${url}/sys/php/auth.php`, {
                action: 'login',
                login: user.login,
                password: user.password,
            }).then((resp) => {
                if (resp.data.status === 'ok') {
                    commit('AUTH_SUCCESS', resp.data.user);
                    res(resp);
                } else {
                    commit('AUTH_FAIL');
                    rej(resp);
                }
            }).catch((err) => {
                commit('AUTH_FAIL');
                rej(err);
            });
        }),
        AUTH_LOGOUT: ({ commit }) => new Promise((res, rej) => {
            axios.post(`${url}/sys/php/auth.php`, {
                action: 'logout',
            }).then((resp) => {
                if (resp.data.status === 'ok') {
                    commit('AUTH_LOGOUT');
                    res(resp);
                } else {
                    rej(resp);
                }
            }).catch((err) => {
                rej(err);
            });
        }),
        GetUser: ({ commit }, id) => new Promise((res, rej) => {
            axios.post(`${url}/sys/php/auth.php`, {
                action: 'getFullUser',
                id,
            }).then((resp) => {
                if (resp.data.status === 'ok' && resp.data.user.length !== 0) {
                    commit('GET_FULL_USER', resp.data.user);
                    res(resp.data.user);
                } else if (resp.data.user.length === 0) {
                    rej(new Error('redir'));
                } else rej(resp.data);
            }).catch((err) => {
                rej(err);
            });
        }),
        GetSelfUser: (store) => {
            axios.post(`${url}/sys/php/auth.php`, {
                action: 'getSelfUser',
            }).then((resp) => {
                if (resp.data.status === 'ok') {
                    if (!myLib.checkObjs(store.state.Auth.User, resp.data.user)) {
                        store.commit('AUTH_SUCCESS', resp.data.user);
                    }
                } else {
                    store.commit('AUTH_FAIL');
                }
            }).catch((err) => {
                console.log(err);
            });
        },
        GetUsersTable: ({ commit }) => {
            axios.post(`${url}/sys/php/auth.php`, {
                action: 'getUserTable',
            }).then((resp) => {
                if (resp.data.status === 'ok') {
                    commit('GET_USERS', resp.data.users);
                } else {
                    console.log(resp);
                }
            }).catch((err) => {
                console.log(err);
            });
        },
    },
});
