export default {
    myConcat(arr1, arr2) {
        const bufArr = arr1.concat(arr2);
        const obj = {};
        bufArr.forEach((elem) => {
            if (!obj[elem.id]) obj[elem.id] = {};
            Object.keys(elem).forEach((key) => {
                obj[elem.id][key] = elem[key];
            });
        });
        const result = [];
        Object.keys(obj).forEach((key) => {
            result.push(obj[key]);
        });
        return result;
    },
    concatObj(obj1, obj2) { // Объединение объектов с сохранение свойств обоих, приоритет второму
        const bufKeys = Object.keys(obj1).concat(Object.keys(obj2));
        const obj = {};
        bufKeys.forEach((key) => {
            obj[key] = true;
        });
        const result = {};
        Object.keys(obj).forEach((key) => {
            result[key] = (obj2[key]) ? obj2[key] : obj1[key];
        });
        return result;
    },
    checkObjs(obj1, obj2) {
        const keys1 = Object.keys(obj1);
        const keys2 = Object.keys(obj2);
        if (keys1.length !== keys2.length) return false;
        return keys1.every(key => obj1[key] === obj2[key]);
    },
    getInf() {
        let result;
        const nullB = {
            id: null,
            login: null,
            rights: null,
        };
        try {
            const ls = JSON.parse(localStorage.getItem('userInf'));
            if (ls) result = ls;
            else result = nullB;
        } catch (e) {
            result = nullB;
        }
        return result;
    },
    setInf(obj) {
        localStorage.setItem('userInf', JSON.stringify(obj));
    },
    delInf() {
        localStorage.removeItem('userInf');
    },
};
